import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../providers/providers';
import { Router } from '@angular/router';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
  
  players: Object;

  constructor(public playerService: PlayerService, private router: Router) { }

  ngOnInit() {
    this.playerService.get().subscribe(players=> this.players = players);
  }

  moreInfo(playerId){
    this.router.navigate([`/players`,`${playerId}`]);
  }

}
