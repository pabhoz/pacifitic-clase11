import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MusicMatchService } from '../providers/providers';

@Component({
  selector: 'app-music-match',
  templateUrl: './music-match.component.html',
  styleUrls: ['./music-match.component.scss']
})
export class MusicMatchComponent implements OnInit {
  query: String;
  results: any;
  constructor(public router: Router, public music: MusicMatchService) { }

  ngOnInit() {
  }

  buscar(){
    this.router.navigate(['/artists',this.query]);
  }

  getFromAPI(){
    this.music.get(this.query).subscribe(
      (response) => {
        this.results = response;
      }
    )
  } 

}
