import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nullAttr'
})
export class NullAttrPipe implements PipeTransform {

  transform(value: any) {
    return (value != null && value != "")? value : "No registra";
  }

}
