import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { rutasApp } from './app.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pacifitic';
  home: boolean;
  menus: any = [];

  constructor(public router: Router) {
    //console.log(rutasApp);
    for (let i = 0; i < rutasApp.length; i++) {
      if (rutasApp[i].name != undefined){
        this.menus.push([rutasApp[i].name, `/${rutasApp[i].path}`]);
      }
    }
    console.log(this.menus);
  }
  
}
