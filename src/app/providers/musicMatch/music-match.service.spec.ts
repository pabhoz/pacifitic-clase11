import { TestBed, inject } from '@angular/core/testing';

import { MusicMatchService } from './music-match.service';

describe('MusicMatchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MusicMatchService]
    });
  });

  it('should be created', inject([MusicMatchService], (service: MusicMatchService) => {
    expect(service).toBeTruthy();
  }));
});
