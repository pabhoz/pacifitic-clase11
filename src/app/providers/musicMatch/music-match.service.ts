import { Injectable } from '@angular/core';
import { MM, headers } from '../providers';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MusicMatchService {

  api = MM;
  service = "artist.search";
  constructor(public http: HttpClient) { }

  get(q){
    let uri = `${this.api}${this.service}`;
    let params = `?page=1&page_size=5&q_artist=${q}&s_artist_rating=desc`;
    return this.http.get(uri+params, { headers: headers });
  }
}
