import { Component, OnInit } from '@angular/core';
import { MatchService } from '../providers/providers';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {

  constructor(private matchService: MatchService) { }

  ngOnInit() {
  }

}
